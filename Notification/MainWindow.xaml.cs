﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Notification.views;
using Notification.misc;
namespace Notification
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly double THRESHOLD1 = 1.4;//ruffly 4ft
        private readonly double THRESHOLD2 = 2.8;//ruffly 8.5ft
        //constant values
        Constants constant = Constants.getInstance();
        //kinect stuff
        private List<Body> bodies;
        private KinectSensor sensor;
        private int[] handState = new int[4]{0, 0, 1, 1};
        private DateTime timer = DateTime.Now;
        //views
        private int range;  //track screen range
        private screens current;
        

        public MainWindow()
        {
            InitializeComponent();
            this.KeyUp += MainWindow_KeyUp;
            //init kinect
            this.sensor = KinectSensor.GetDefault();
            this.sensor.Open();
            this.sensor.BodyFrameSource.OpenReader().FrameArrived+=MainWindow_FrameArrived;

            range = 2; //set current screen to far
            current = constant.getScreen(2);
            viewHolder.Children.Add((UserControl)current);
            callHolder.Children.Add(new call_view());
            constant.changeConnectStatus(0);
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.N: //no device
                    constant.changeConnectStatus(0);
                    break;
                case Key.C: //device present
                    constant.changeConnectStatus(1);
                    break;
                case Key.M: //incoming call
                    callHolder.Visibility = Visibility.Visible;
                    break;
                case Key.E: //new event
                    constant.changeEventStatus(1);
                    break;
                case Key.S: //new message
                    constant.changeEventStatus(2);
                    break;
            }
        }

        private void MainWindow_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            Body trackedBody = null;
            double min_distance = 100;//reset min
            BodyFrame frame = e.FrameReference.AcquireFrame();
            using (frame)
            {
                if (frame != null)
                {
                    this.bodies = new Body[frame.BodyFrameSource.BodyCount].ToList();
                    frame.GetAndRefreshBodyData(bodies);
                    foreach (Body body in bodies)
                    {
                        if (body.IsTracked)
                        {
                            //find the closest distance
                            if (body.Joints[JointType.SpineBase].Position.Z < min_distance)
                            {
                                trackedBody = body;
                                min_distance = body.Joints[JointType.SpineBase].Position.Z;
                            }
                        }
                    }
                }
            }
            calculateHand(trackedBody);
            calculateRange(min_distance);
        }

        private void calculateHand(Body body)
        {
            double leftArm, rightArm, leftHand, rightHand;
            int rightState = handState[0], leftState = handState[1];
            int rightSpread = handState[2], leftSpread = handState[3];
            Dictionary<int, int> states = new Dictionary<int, int>();
            bool isTimePassed = (DateTime.Now - timer).Milliseconds > 800;

            if (body != null)
            {  //there actually is a body and current screen is not far screen
                //calculate arm distance
                leftArm = (Math.Abs(body.Joints[JointType.WristLeft].Position.X) - Math.Abs(body.Joints[JointType.ElbowLeft].Position.X))
                    + (Math.Abs(body.Joints[JointType.ElbowLeft].Position.X) - Math.Abs(body.Joints[JointType.ShoulderLeft].Position.X));
                rightArm = (Math.Abs(body.Joints[JointType.WristRight].Position.X) - Math.Abs(body.Joints[JointType.ElbowRight].Position.X))
                    + (Math.Abs(body.Joints[JointType.ElbowRight].Position.X) - Math.Abs(body.Joints[JointType.ShoulderRight].Position.X));
                //calcualte hand distance to shoulder
                leftHand = Math.Abs(body.Joints[JointType.HandLeft].Position.X) - Math.Abs(body.Joints[JointType.ShoulderLeft].Position.X);
                rightHand = Math.Abs(body.Joints[JointType.HandRight].Position.X) - Math.Abs(body.Joints[JointType.ShoulderRight].Position.X);

                //see if any arm is spread out
                if ((leftHand > leftArm) && (rightHand <= rightArm)) { leftSpread = 0; }//left is out
                else if ((leftHand <= leftArm) && (rightHand > rightArm)) { rightSpread = 0; }//right is out
                else{  //both or none of them is out
                    rightSpread = 1;
                    leftSpread = 1;
                }

                // Find the right hand state
                switch (body.HandRightState)
                {
                    case HandState.Open:
                        rightState = 0;
                        break;
                    case HandState.Closed:
                        rightState = 1;
                        break;
                    default:
                        break;
                }

                // Find the left hand state
                switch (body.HandLeftState)
                {
                    case HandState.Open:
                        leftState = 0;
                        break;
                    case HandState.Closed:
                        leftState = 1;
                        break;
                    default:
                        break;
                }
            }
            
            //find if any value changed
            if ((rightState != handState[0]) && isTimePassed){
                handState[0] = rightState;
                states[0] = rightState;
                timer = DateTime.Now;
            }
            if ((leftState != handState[1]) && isTimePassed){
                handState[1] = leftState;
                states[1] = leftState;
                timer = DateTime.Now;
            }
            if (rightSpread != handState[2]){
                handState[2] = rightSpread;
                states[2] = rightSpread;
            }
            if (leftSpread != handState[3]){
                handState[3] = leftSpread;
                states[3] = leftSpread;
            }
            if (states.Count != 0) { updateScreen(states); }
        }

        private void updateScreen(Dictionary<int, int> states)
        {
            if (callHolder.Visibility == Visibility.Visible && (states.ContainsKey(0) || states.ContainsKey(1)))
            {//pick the call have hight priority than other
                //if right hand closed, pick the call
                if (states.ContainsKey(0) && states[0] == 1){constant.changeCallStatus(1);}
                //no matter what, change call view back to invisible
                callHolder.Visibility = Visibility.Collapsed;
                return;
            }
            current.inputGesture(states);
        }


        private void calculateRange(double distance)
        {
            int range = this.range;
            //normalize min distance
            Math.Round(distance, 1);
            //screen will not be changed if min is in the same range as previous
            //close range
            if (distance < THRESHOLD1) { range = 0; }
            //mid range
            else if (distance >= THRESHOLD1 && distance < THRESHOLD2) { range = 1; }
            //far range
            else { range = 2; }

            if (range != this.range)
            {//if current is not in apporiate range, set to that range
                this.range = range;
                if (this.range == 0 && constant.getConnectionStatus() == 1)
                {//at correct range to change connection to connected
                    constant.changeConnectStatus(2);
                }
                changeScreen();
            }
        }

        private async void changeScreen()
        {
            current.hide();//display hide animation for the previous current
            await Task.Delay(600);
            viewHolder.Children.Clear();
            current = constant.getScreen(range);
            if (range == 0) { ((near_view)current).setItem(((mid_view)constant.getScreen(1)).getItem()); }
            viewHolder.Children.Add((UserControl)current);
            current.show();//display show animation for the current screen
        }
    }
}
