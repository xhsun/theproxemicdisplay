﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notification.misc
{
    class interruptEventListener
    {
        private ArrayList listener = new ArrayList();

        public void onConnectionChange(object sender, int status)
        {
            foreach (interruptEvent listen in listener)
            {
                listen.onConnectionChange(status);
            }
        }

        public void onIncomingCall(object sender, int status)
        {
            foreach (interruptEvent listen in listener)
            {
                listen.onIncomingCall(status);
            }
        }

        public void onNewEvent(object sender, int status)
        {
            foreach (interruptEvent listen in listener)
            {
                listen.onNewEvent(status);
            }
        }

        public void attach(interruptEvent listen)
        {
            listener.Add(listen);
        }
        public void detach(interruptEvent listen)
        {
            listener.Remove(listen);
        }
    }
}
