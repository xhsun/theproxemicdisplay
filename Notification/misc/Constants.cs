﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Notification.views;
using System.Windows.Media.Imaging;
namespace Notification.misc
{

    class Constants
    {
        private int status;
        private static Constants constant;
        private screens[] screens = new screens[3];
        private interruptEventListener listener;
        //events
        private delegate void onConnectChangeHandler(object sender, int value);
        private event onConnectChangeHandler connect = delegate { };
        private delegate void onIncomingCallHandler(object sender, int value);
        private event onIncomingCallHandler call = delegate { };
        private delegate void onNewEventHandler(object sender, int value);
        private event onNewEventHandler newevent = delegate { };
        //info
        private Dictionary<int, info> music = new Dictionary<int, info>();
        private Dictionary<int, info> security = new Dictionary<int, info>();
        private Dictionary<int, info> message = new Dictionary<int, info>();

         /// <summary>
        /// get instance of this class
        /// since this class is singleton, constructor will not be available
        /// </summary>
        /// <returns>instance of this class</returns>
        public static Constants getInstance()
        {
            if (constant == null) { constant = new Constants(); }
            return constant;
        }

        private Constants()
        {
            status = -1;
            screens[0] = new near_view();
            screens[1] = new mid_view();
            screens[2] = new far_view();
            //listeners
            listener = new interruptEventListener();
            this.call += listener.onIncomingCall;
            this.newevent += listener.onNewEvent;
            this.connect += listener.onConnectionChange;
            listener.attach((interruptEvent)screens[0]);
            listener.attach((interruptEvent)screens[1]);
            listener.attach((interruptEvent)screens[2]);
            initInfos();
        }

        public screens getScreen(int range)
        {
            //return far view if range is out of bound
            if (range < 0 && range > 2) { return screens[2]; }
            return screens[range];
        }

        public int getConnectionStatus()
        {
            return this.status;
        }

        /// <summary>
        /// change the status of device connection
        /// </summary>
        /// <param name="status">0 - no connection, 1 - device detected, 2 - connected</param>
        public void changeConnectStatus(int status)
        {
            if (status != this.status)
            {
                this.status = status;
                connect(this, this.status);
            }
        }

        /// <summary>
        /// change the status of call
        /// </summary>
        /// <param name="status">1 - picked up call, 2 - call ended</param>
        public void changeCallStatus(int status)
        {
            if (status != this.status)
            {
                this.status = status;
                call(this, this.status);
            }
        }

        /// <summary>
        /// notify the type of new event
        /// </summary>
        /// <param name="status">1 - events, 2 - message</param>
        public void changeEventStatus(int status)
        {
            if (status != this.status)
            {
                this.status = status;
                newevent(this, this.status);
            }
        }

        private void initInfos()
        {
            info music1 = new info();
            music1.title = "twinkle twinkle little star";
            music1.add1 = "Jane Taylor";
            music1.add2 = "Unknown";
            music1.add3 = "Twinkle, twinkle, little star, \n How I wonder what you are...";
            music1.body = "Twinkle, twinkle, little star, \n How I wonder what you are. \n Up above the world so high, \n Like a diamond in the sky.";
            music[0] = music1;

            info music2 = new info();
            music2.title = "Santa Claus Is Coming to Town";
            music2.add1 = "J. Fred Coots and Haven Gillespie";
            music2.add2 = "Unknown";
            music2.add3 = "You better watch out \n You better not cry...";
            music2.body = "You better watch out \n You better not cry \n Better not pout \n I'm telling you why \n Santa Claus is coming to town";
            music[1] = music2;

            info sec1 = new info();
            sec1.title = "Home mode";
            sec1.image = new BitmapImage(new Uri(@"../image/home.png", UriKind.Relative));
            sec1.body = "Only door and window sensor will be turned on";
            security[0] = sec1;

            info sec2 = new info();
            sec2.title = "Lock mode";
            sec2.image = new BitmapImage(new Uri(@"../image/lock.png", UriKind.Relative));
            sec2.body = "All sensor will be turned on";
            security[1] = sec2;

            info sec3 = new info();
            sec3.title = "Unlock mode";
            sec3.image = new BitmapImage(new Uri(@"../image/unlock.png", UriKind.Relative));
            sec3.body = "All sensor will be turned off";
            security[2] = sec3;

            info msg1 = new info();
            msg1.title = "[SPAM:#] Get your tax refund now";
            msg1.add1 = "After the last annual calculations of your account activity we have determined that you are eligible to receive a tax refund of...";
            msg1.body = "After the last annual calculations of your account activity we have determined that you are eligible to receive a tax refund of $479.30 . \n Please submit the tax refund request and allow us 2-6 days in order to process it. \n A refund can be delayed for a variety of reasons. \n For example submitting invalid records or applying after the deadline....";
            message[0] = msg1;

            info msg2 = new info();
            msg2.title = "New text message from Daily Cat Fact";
            msg2.add1 = "Cat use their tails for balance and have....";
            msg2.body = "Cat use their tails for balance and have nearly 30 individual bones in them!";
            message[1] = msg2;
        }

        public Dictionary<int, info> getInfo(string name)
        {
            switch (name)
            {
                case"Music":
                    return music;
                case "Security":
                    return security;
                case "Message":
                    return message;
            }
            return null;
        }
    }
}
