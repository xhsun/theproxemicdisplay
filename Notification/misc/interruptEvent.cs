﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notification.misc
{
    interface interruptEvent
    {
        /// <summary>
        /// notify for phone connection change
        /// </summary>
        /// <param name="status">0 - no connection, 1 - device detected, 2 - connected</param>
        void onConnectionChange(int status);
        /// <summary>
        /// notify screens for incoming call
        /// </summary>
        /// <param name="status">1 - picked up call, 2 - call ended</param>
        void onIncomingCall(int status);
        /// <summary>
        /// notify screens for new events
        /// </summary>
        /// <param name="status">1 - events, 2 - message</param>
        void onNewEvent(int status);
    }
}
