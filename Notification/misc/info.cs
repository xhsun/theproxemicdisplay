﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Notification.misc
{
    class info
    {
        public string title { get; set; }
        public string body { get; set; }
        public BitmapImage image { get; set; }
        public string add1 { get; set; }
        public string add2 { get; set; }
        public string add3 { get; set; }
    }
}
