﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notification.views
{
    interface screens
    {
        void show();
        void hide();
        void inputGesture(Dictionary<int, int> handstates);
    }
}
