﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Notification.misc;
using System.Windows.Media.Animation;
namespace Notification.views
{
    /// <summary>
    /// Interaction logic for mid_view.xaml
    /// </summary>
    public partial class mid_view : UserControl, screens, interruptEvent
    {
        private string category = "Music";
        private bool needNotify = false;
        private Dictionary<string, int> items = new Dictionary<string, int>();
        private Dictionary<int, string> categories = new Dictionary<int, string>();
        //animation
        private Storyboard exiting;
        private Storyboard enter;
        public mid_view()
        {
            InitializeComponent();
            items["Music"] = 0;
            items["Security"] = 0;
            items["Message"] = 0;
            items["Event"] = 0;
            
            exiting = this.Resources["screen_exit"] as Storyboard;
            enter = this.Resources["screen_enter"] as Storyboard;
        }

        public KeyValuePair<string, int> getItem()
        {
            return new KeyValuePair<string, int>(category, items[category]);
        }

        public void show(){
            enter.Begin();
            showItem(true);
        }

        public void hide(){
            exiting.Begin();
            showItem(false);
        }

        public async void inputGesture(Dictionary<int, int> handstates)
        {
            int key;
            if (handstates.ContainsKey(0) || handstates.ContainsKey(1))
            {
                if(handstates.ContainsKey(0) && handstates[0] == 1){//right hand for right
                    rightarrow.Opacity = 50;
                    key = categories.FirstOrDefault(x => x.Value == category).Key;
                    category = ((key + 1) > (categories.Count - 1)) ? categories[0] : categories[key + 1];
                    await Task.Delay(400);
                    rightarrow.Opacity = 10;
                    showItem(true);
                }
                else if (handstates.ContainsKey(1) && handstates[1] == 1)//left hand for left
                {
                    leftarrow.Opacity = 50;
                    key = categories.FirstOrDefault(x => x.Value == category).Key;
                    category = ((key - 1) < 0) ? categories[categories.Count-1] : categories[key - 1];
                    await Task.Delay(400);
                    leftarrow.Opacity = 10;
                    showItem(true);
                }
            }
            else if (handstates.ContainsKey(2) || handstates.ContainsKey(3))
            {
                if (handstates.ContainsKey(2) && handstates[2] == 0){//right arm for right
                    righthand.Opacity = 50;
                    switch (category)
                    {
                        case "Music":
                        case "Message":
                            items[category] = (items[category] + 1 > 1) ? 0 : 1;
                            break;
                        case "Security":
                            items[category] = (items[category] + 1 > 2) ? 0 : items[category] + 1;
                            break;
                        default:
                            break;
                    }
                    await Task.Delay(400);
                    righthand.Opacity = 10;
                    showItem(true);
                }
                else if (handstates.ContainsKey(3) && handstates[3] == 0)//left arm for left
                {
                    lefthand.Opacity = 50;
                    switch (category)
                    {
                        case "Music":
                        case "Message":
                            items[category] = (items[category] - 1 < 0) ? 1 : 0;
                            break;
                        case "Security":
                            items[category] = (items[category] - 1 < 0) ? 2 : items[category] - 1;
                            break;
                        default:
                            break;
                    }
                    await Task.Delay(400);
                    lefthand.Opacity = 10;
                    showItem(true);
                }
            }
        }

        public void onConnectionChange(int status)
        {
            categories = new Dictionary<int, string>();
            switch (status)
            {
                case 0: //no connection
                    categories[0] = "Music";
                    categories[1] = "Security";
                    category = "Music";
                    items[category] = 0;
                    showItem(true);
                    break;
                case 1: //device detected
                    category = "Detected";
                    showItem(true);
                    break;
                case 2: //connected
                    category = "Music";
                    categories[0] = "Music";
                    categories[1] = "Message";
                    categories[2] = "Security";
                    categories[3] = "Event";
                    needNotify = true;
                    notify_img.Source = new BitmapImage(new Uri(@"../image/timer.png", UriKind.Relative));
                    notify1.Text = "Laundry Ready";
                    notify2.Text = "1:30 hours";
                    showItem(true);
                    break;
                default: //error
                    break;
            }
        }

        public void onIncomingCall(int status)
        {
            needNotify = true;
            switch (status)
            {
                case 1: //call
                    notify_img.Source = new BitmapImage(new Uri(@"../image/call.png", UriKind.Relative));
                    notify1.Text = "Mom";
                    notify2.Text = "2:00";
                    break;
                case 2: //end call
                    notify_img.Source = new BitmapImage(new Uri(@"../image/timer.png", UriKind.Relative));
                    notify1.Text = "Laundry Ready";
                    notify2.Text = "1:30 hours";
                    break;
            }
            notification.Visibility = Visibility.Visible;
        }

        public void onNewEvent(int status)
        {
            switch (status)
            {
                case 1://event
                    category = "Event";
                    showItem(true);
                    break;
                case 2://message
                    category = "Message";
                    items[category] = 1;
                    showItem(true);
                    break;
            }
        }

        private void showItem(bool isShow)
        {
            Constants constant = Constants.getInstance();
            //hide everything first
            detected.Visibility = Visibility.Collapsed;
            music.Visibility = Visibility.Collapsed;
            message.Visibility = Visibility.Collapsed;
            events.Visibility = Visibility.Collapsed;
            security.Visibility = Visibility.Collapsed;
            notification.Visibility = Visibility.Collapsed;
            if (!isShow) { return; }
            if (needNotify) { notification.Visibility = Visibility.Visible; }
            title.Text = category;
            switch (category)
            {
                case "Music":
                    music_title.Text = constant.getInfo(category)[items[category]].title;
                    music_artist.Text ="Artist: " + constant.getInfo(category)[items[category]].add1;
                    music_album.Text = "Album: " + constant.getInfo(category)[items[category]].add2;
                    music_lyric.Text = constant.getInfo(category)[items[category]].add3.Replace("\n", Environment.NewLine);
                    music.Visibility = Visibility.Visible;
                    break;
                case "Message":
                    msg_title.Text = constant.getInfo(category)[items[category]].title;
                    msg_body.Text = constant.getInfo(category)[items[category]].add1;
                    message.Visibility = Visibility.Visible;
                    break;
                case "Event":
                    events.Visibility = Visibility.Visible;
                    break;
                case "Security":
                    security_title.Text=constant.getInfo(category)[items[category]].title;
                    security_icon.Source=constant.getInfo(category)[items[category]].image;
                    security_body.Text = constant.getInfo(category)[items[category]].body;
                    security.Visibility = Visibility.Visible;
                    break;
                case "Detected":
                    detected.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
        }
    }
}
