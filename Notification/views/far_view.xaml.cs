﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Notification.misc;
using System.Windows.Media.Animation;
namespace Notification.views
{
    /// <summary>
    /// Interaction logic for far_view.xaml
    /// </summary>
    public partial class far_view : UserControl, screens, interruptEvent
    {
        //index: 0 - top on going event, 1 - second top, 2 - backup
        //value: -1 - empty, 0 - device detected, 1 - call, 2 - music, 3 - timer
        private int[] eventList = new int[3]{-1, -1, -1};
        //animations
        private Storyboard enter;
        private Storyboard exiting;
        private Storyboard newEvent;
        private Storyboard newMessage;
        private Storyboard showOngoing1;
        private Storyboard showOngoing2;
        public far_view()
        {
            InitializeComponent();
            showOngoing1 = this.Resources["show_ongoing1"] as Storyboard;
            showOngoing2 = this.Resources["show_ongoing2"] as Storyboard;
            enter = this.Resources["screen_enter"] as Storyboard;
            exiting = this.Resources["screen_exit"] as Storyboard;
            newEvent = this.Resources["event_highlight"] as Storyboard;
            newMessage = this.Resources["msg_highlight"] as Storyboard;
        }

        public void changeSecurityMode(int mode)
        {
            switch (mode)
            {
                case 0: //home mode
                    security_img.Source = new BitmapImage(new Uri(@"../image/home.png", UriKind.Relative));
                    break;
                case 1: //lock mode
                    security_img.Source = new BitmapImage(new Uri(@"../image/lock.png", UriKind.Relative));
                    break;
                case 2: //unlock mode
                    security_img.Source = new BitmapImage(new Uri(@"../image/unlock.png", UriKind.Relative));
                    break;
                default:
                    break;
            }
        }

        public void show(){enter.Begin();}

        public void hide()
        {
            event_notify.Visibility = Visibility.Collapsed;
            msg_notify.Visibility = Visibility.Collapsed;
            exiting.Begin();
        }

        public void inputGesture(Dictionary<int, int> handstates)
        {
            if (handstates.ContainsKey(0) && handstates[0] == 1)//right hand
            {
                if (eventList[0] == 2) { ongoing_txt1_2.Text = (ongoing_txt1_2.Text == "1:10 / 2:45") ? "Paused" : "1:10 / 2:45"; }
                else if (eventList[1] == 2) { ongoing_txt2_2.Text = (ongoing_txt2_2.Text == "1:10 / 2:45") ? "Paused" : "1:10 / 2:45"; }
                else if (eventList[0] == -1)
                {
                    eventList[0] = 2;
                    ongoing_img1.Source = new BitmapImage(new Uri(@"../image/music.png", UriKind.Relative));
                    ongoing_txt1_1.Text = "Unnamed music 1";
                    ongoing_txt1_2.Text = "1:10 / 2:45";
                    ongoing_1.Visibility = Visibility.Visible;
                    showOngoing1.Begin();
                }
                else if (eventList[1] == -1)
                {
                    eventList[1] = 2;
                    ongoing_img2.Source = new BitmapImage(new Uri(@"../image/music.png", UriKind.Relative));
                    ongoing_txt2_1.Text = "Unnamed music 1";
                    ongoing_txt2_2.Text = "1:10 / 2:45";
                    ongoing_2.Visibility = Visibility.Visible;
                    showOngoing2.Begin();
                }
                else if (eventList[0] != 0 && eventList[0] != 1)
                {
                    eventList[1] = eventList[0];
                    ongoing_img2.Source = ongoing_img1.Source;
                    ongoing_txt2_1.Text = ongoing_txt1_1.Text;
                    ongoing_txt2_2.Text = ongoing_txt1_2.Text;
                    ongoing_2.Visibility = Visibility.Visible;
                    showOngoing2.Begin();

                    eventList[0] = 2;
                    ongoing_img1.Source = new BitmapImage(new Uri(@"../image/music.png", UriKind.Relative));
                    ongoing_txt1_1.Text = "Unnamed music 1";
                    ongoing_txt1_2.Text = "1:10 / 2:45";
                    ongoing_1.Visibility = Visibility.Visible;
                }
                else
                {
                    eventList[1] = 2;
                    ongoing_img2.Source = new BitmapImage(new Uri(@"../image/music.png", UriKind.Relative));
                    ongoing_txt2_1.Text = "Unnamed music 1";
                    ongoing_txt2_2.Text = "1:10 / 2:45";
                    showOngoing2.Begin();
                }
            }
            else if (handstates.ContainsKey(1) && handstates[1] == 1) //left hand
            {
                if (eventList[0] == 1)
                {
                    Constants constant = Constants.getInstance();
                    constant.changeCallStatus(2);
                }
            }
        }
    
        public void onConnectionChange(int status)
        {
            switch (status)
            {
                case 0: //no connection
                    events.Visibility = Visibility.Collapsed;
                    msg.Visibility = Visibility.Collapsed;
                    list.Visibility = Visibility.Collapsed;
                    ongoing_1.Visibility = Visibility.Collapsed;
                    ongoing_2.Visibility = Visibility.Collapsed;
                    eventList = new int[3] { -1, -1, -1 };
                    break;
                case 1: //device detected
                    if (ongoing_1.Visibility == Visibility.Visible)
                    {
                        eventList[1] = eventList[0];
                        eventList[2] = -1;
                        ongoing_img2.Source = ongoing_img1.Source;
                        ongoing_txt2_1.Text = ongoing_txt1_1.Text;
                        ongoing_txt2_2.Text = ongoing_txt1_2.Text;
                        ongoing_2.Visibility = Visibility.Visible;
                    }
                    eventList[0] = 0;
                    ongoing_img1.Source = new BitmapImage(new Uri(@"../image/phone.png", UriKind.Relative));
                    ongoing_txt1_1.Text = "Device detected";
                    ongoing_txt1_2.Text = "Step forward to connect";
                    ongoing_1.Visibility = Visibility.Visible;
                    showOngoing1.Begin();
                    showOngoing2.Begin();
                    break;
                case 2: //connected
                    if (eventList[1] == -1)
                    {//nothing going on for the second slot
                        eventList[0] = 3;
                        ongoing_img1.Source = new BitmapImage(new Uri(@"../image/timer.png", UriKind.Relative));
                        ongoing_txt1_1.Text = "Laundry Ready";
                        ongoing_txt1_2.Text = "1:30 hours";
                        showOngoing1.Begin();
                    }
                    else if (eventList[1] != -1)
                    {//there is something going on
                        eventList[0] = eventList[1];
                        ongoing_img1.Source = ongoing_img2.Source;
                        ongoing_txt1_1.Text = ongoing_txt2_1.Text;
                        ongoing_txt1_2.Text = ongoing_txt2_2.Text;

                        ongoing_img2.Source = new BitmapImage(new Uri(@"../image/timer.png", UriKind.Relative));
                        ongoing_txt2_1.Text = "Laundry Ready";
                        ongoing_txt2_2.Text = "1:30 hours";
                    }
                    eventList[2] = -1;
                    events.Visibility = Visibility.Visible;
                    msg.Visibility = Visibility.Visible;
                    list.Visibility = Visibility.Visible;
                    break;
                default: //error
                    break;
            }
        }

        public void onIncomingCall(int status)
        {
            switch (status)
            {
                case 1: //call
                    if (eventList[0] != -1)
                    {
                        eventList[2] = eventList[1];
                        eventList[1] = eventList[0];
                        ongoing_img2.Source = ongoing_img1.Source;
                        ongoing_txt2_1.Text = ongoing_txt1_1.Text;
                        ongoing_txt2_2.Text = ongoing_txt1_2.Text;
                        ongoing_2.Visibility = Visibility.Visible;
                        showOngoing2.Begin();
                    }
                    eventList[0] = 1;
                    ongoing_img1.Source = new BitmapImage(new Uri(@"../image/call.png", UriKind.Relative));
                    ongoing_txt1_1.Text = "Mom";
                    ongoing_txt1_2.Text = "2:00";
                    ongoing_1.Visibility = Visibility.Visible;
                    showOngoing1.Begin();
                    break;
                case 2: //end call
                    if (eventList[1] != -1)
                    {
                        eventList[0] = eventList[1];
                        ongoing_img1.Source = ongoing_img2.Source;
                        ongoing_txt1_1.Text = ongoing_txt2_1.Text;
                        ongoing_txt1_2.Text = ongoing_txt2_2.Text;
                        switch (eventList[2])
                        {
                            case 2:
                                ongoing_img2.Source = new BitmapImage(new Uri(@"../image/music.png", UriKind.Relative));
                                ongoing_txt2_1.Text = "Unnamed music 1";
                                ongoing_txt2_2.Text = "1:10 / 2:45";
                                break;
                            case 3:
                                ongoing_img2.Source = new BitmapImage(new Uri(@"../image/timer.png", UriKind.Relative));
                                ongoing_txt2_1.Text = "Laundry Ready";
                                ongoing_txt2_2.Text = "1:30 hours";
                                break;
                            case -1:
                                ongoing_2.Visibility = Visibility.Collapsed;
                                break;
                        }
                        eventList[1] = eventList[2];
                        eventList[2] = -1;
                    }
                    break;
            }
        }

        public void onNewEvent(int status)
        {
            switch (status)
            {
                case 1://event
                    event_notify.Visibility = Visibility.Visible;
                    newEvent.Begin();
                    break;
                case 2://message
                    msg_notify.Visibility = Visibility.Visible;
                    newMessage.Begin();
                    break;
            }
        }
    }
}
