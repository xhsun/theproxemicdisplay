﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Notification.misc;
namespace Notification.views
{
    /// <summary>
    /// Interaction logic for near_view.xaml
    /// </summary>
    public partial class near_view : UserControl, screens, interruptEvent
    {
        private string category;
        private int item;
        private bool status=false, needNotify;
        public near_view()
        {
            InitializeComponent();
        }

        public void setItem(KeyValuePair<string, int> item)
        {
            category = item.Key;
            this.item = item.Value;
        }

        public void show()
        {
            if (status) {
                status = false;
                detected.Visibility = Visibility.Visible;
                return; 
            }
            showItem(true);
        }

        public void hide()
        {
            showItem(false);
        }

        public void inputGesture(Dictionary<int, int> handstates)
        {
            if (handstates.ContainsKey(1) && handstates[1] == 1) //left hand
            {
                needNotify = false;
                notification.Visibility = Visibility.Collapsed;
            }
        }

        public void onConnectionChange(int status)
        {
            if (status == 1) { this.status = true;}
            showItem(true);
        }

        public void onIncomingCall(int status)
        {
            needNotify = true;
            switch (status)
            {
                case 1: //call
                    notify_img.Source = new BitmapImage(new Uri(@"../image/call.png", UriKind.Relative));
                    notify1.Text = "Mom";
                    notify2.Text = "2:00";
                    notification.Visibility = Visibility.Visible;
                    break;
                case 2: //end call
                    needNotify = false;
                    notification.Visibility = Visibility.Collapsed;
                    break;
            }
            
        }

        public void onNewEvent(int status)
        {
        }

        private void showItem(bool isShow)
        {
            Constants constant = Constants.getInstance();
            //hide everything first
            detected.Visibility = Visibility.Collapsed;
            music.Visibility = Visibility.Collapsed;
            message.Visibility = Visibility.Collapsed;
            events.Visibility = Visibility.Collapsed;
            security.Visibility = Visibility.Collapsed;
            notification.Visibility = Visibility.Collapsed;
            if (!isShow) { return; }
            if (needNotify) { notification.Visibility = Visibility.Visible; }
            title.Text = category;
            switch (category)
            {
                case "Music":
                    music_title.Text = constant.getInfo(category)[item].title;
                    music_artist.Text = "Artist: " + constant.getInfo(category)[item].add1;
                    music_album.Text = "Album: " + constant.getInfo(category)[item].add2;
                    music_lyric.Text = constant.getInfo(category)[item].body.Replace("\n", Environment.NewLine);
                    music.Visibility = Visibility.Visible;
                    break;
                case "Message":
                    title.Text = constant.getInfo(category)[item].title;
                    content.Text = constant.getInfo(category)[item].body;
                    message.Visibility = Visibility.Visible;
                    break;
                case "Event":
                    events.Visibility = Visibility.Visible;
                    break;
                case "Security":
                    security_title.Text = constant.getInfo(category)[item].title;
                    security_icon.Source = constant.getInfo(category)[item].image;
                    security_body.Text = constant.getInfo(category)[item].body;
                    security.Visibility = Visibility.Visible;
                    break;
                case "Detected":
                    detected.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
        }
    }
}
