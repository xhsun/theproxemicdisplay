# README #
Project 3: The Proxemic Display

### What is this repository for? ###

This repository contains the prototype implementation of the proxemic display.
For more detail on the design and walkthrough of the proxemic display, please visit [my web portfolio](http://pages.cpsc.ucalgary.ca/~xhsun/work/proxemic.html)

Note: this project make use of the Kinect for the proxemic and gesture input

### How do I get set up? ###

* You can clone or download the repository to access it from your local machine
* link for clone: git clone https://xhsun@bitbucket.org/xhsun/theproxemicdisplay.git
* link for download: [link](https://bitbucket.org/xhsun/theproxemicdisplay/get/a3ac67342748.zip)
* note: if you decide to download the repository, the link for download will provide you with a zip that contain the source code

####To Run The Program####

* Open your visual studio and from there open the project. Before you start the project, make sure your Kinect is plug in and running. After everything is all set up, you can hit start and start the display.

### Contribution guidelines ###

* Author: Hannah Sun

### Who do I talk to? ###

* Hannah Sun: xhsun@ucalgary.ca